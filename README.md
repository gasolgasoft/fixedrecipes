###Fixed Recipes

Fixes some recipes in Tekkit. Conversion between different mods and balancing of recipes.
IndustrialCraft 2 dusts and ThermalExpansion's dusts is convertible in a Crafting Table.

######Nether Star
![Nether Star Recipe](http://doverfelt.se/dl/NetherStarRecipe.jpg)

A regular Minecraft Nether Star.

######UU-Matter Block
![UU-Matter Block Recipe](http://doverfelt.se/dl/UUMatterBlockRecipe.jpg)

Is used for decoration, and for recipes in the future.

######Glowstone
![Glowstone Recipe](http://doverfelt.se/dl/GlowstoneRecipe.jpg)

######Redstone
![Redstone Recipe](http://doverfelt.se/dl/RedstoneRecipe.jpg)

######Certuz Quartz
![Certuz Quartz Recipe](http://doverfelt.se/dl/CertuzRecipe.jpg)

#####Currently working on
* A charging block for charging of electric items using IC2-energy