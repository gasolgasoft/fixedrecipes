package se.doverfelt.FixedRecipes.items;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import se.doverfelt.FixedRecipes.ModInfo;

/**
 * © Rickard Doverfelt 2014
 */
public class ItemCandy extends ItemFood {
    public ItemCandy(int id, int par2, boolean par3) {
        super(id, par2, par3);

    }

    @Override
    public void registerIcons(IconRegister reg) {
       this.itemIcon = reg.registerIcon(ModInfo.ID + ":itemCandy");
    }

    @Override
    public ItemStack onEaten(ItemStack stack, World world, EntityPlayer player) {

        player.addPotionEffect(new PotionEffect(Potion.moveSpeed.getId(), 5, 3));

        return super.onEaten(stack, world, player);
    }

}
