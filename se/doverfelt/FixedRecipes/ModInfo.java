package se.doverfelt.FixedRecipes;

/**
 * © Rickard Doverfelt 2014
 */
public class ModInfo {

    public static final String ID = "fixedRecipes";
    public static final String VERSION = "0.2.2-Dev1";
    public static final String NAME = "Fixed Recipes";

    public static final String CHANNEL = "fixedRecipesChannel";

}
