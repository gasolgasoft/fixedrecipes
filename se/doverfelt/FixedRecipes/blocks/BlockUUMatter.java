package se.doverfelt.FixedRecipes.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
/**
 * © Rickard Doverfelt 2014
 */
public class BlockUUMatter extends Block {

    public BlockUUMatter(int par1, Material par2Material, String name) {
        super(par1, par2Material);
        this.setCreativeTab(CreativeTabs.tabBlock);
        this.setHardness(1f);
        this.setStepSound(stone.stepSound);
        this.setUnlocalizedName(name);
    }

    public void registerIcons(IconRegister reg) {
        this.blockIcon = reg.registerIcon("fixedrecipes:blockUUMatter");
    }

}
