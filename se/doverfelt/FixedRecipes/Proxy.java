package se.doverfelt.FixedRecipes;

/**
 * © Rickard Doverfelt 2014
 */
public class Proxy {

    // Client stuff
    public void registerRenderers() {
        // Nothing here as the server doesn't render graphics or entities!
    }

}
