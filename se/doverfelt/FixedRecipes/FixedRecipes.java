package se.doverfelt.FixedRecipes;

import appeng.api.Materials;
import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import ic2.api.item.Items;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import se.doverfelt.FixedRecipes.blocks.BlockUUMatter;
import se.doverfelt.FixedRecipes.items.ItemCandy;
import thermalexpansion.api.item.ItemRegistry;

/**
 * © Rickard Doverfelt 2014
 */

@Mod(modid = ModInfo.ID, name = ModInfo.NAME, version = ModInfo.VERSION)
@NetworkMod(clientSideRequired=true, channels = {ModInfo.CHANNEL})
public class FixedRecipes {

    @Mod.Instance(value = ModInfo.ID)
    public static FixedRecipes instance;

    @SidedProxy(serverSide = "se.doverfelt.FixedRecipes.Proxy", clientSide = "se.doverfelt.FixedRecipes.ClientProxy")
    public static Proxy proxy;

    public static BlockUUMatter blockUUMatter;

    public static final String COLOUR_PURPLE = "\u00A75";
    public static final String COLOUR_CYAN = "\u00A73";

    public static ItemCandy itemCandy;

    @Mod.PreInit
    public void preInit(FMLPreInitializationEvent event) {

    }

    private void convertRecipe(ItemStack i, ItemStack j) {
        GameRegistry.addShapelessRecipe(i, j);
        GameRegistry.addShapelessRecipe(j, i);
    }

    @Mod.Init
    public void init(FMLInitializationEvent event) {

        blockUUMatter = new BlockUUMatter(160, Material.rock, "blockUUMatter");
        itemCandy = new ItemCandy(10501, 0, false);

        proxy.registerRenderers();
        GameRegistry.addSmelting(Item.netherQuartz.itemID, Materials.matQuartz.copy(), 0.1f);

        GameRegistry.addRecipe(new ItemStack(Item.netherStar), new Object[] { "UUU", "DED", "UUU",
                'U', Items.getItem("matter").copy().getItem(), 'D', Item.diamond, 'E', Item.emerald});

        GameRegistry.addRecipe(new ItemStack(blockUUMatter), new Object[] { "UUU", "U U", "UUU",
                'U', Items.getItem("matter").copy().getItem()});

        GameRegistry.addShapelessRecipe(new ItemStack(Items.getItem("matter").copy().getItem(), 8), new ItemStack(blockUUMatter));

        GameRegistry.addShapelessRecipe(new ItemStack(Item.redstone, 8), new ItemStack(Item.ingotIron), new ItemStack(Item.lightStoneDust));
        GameRegistry.addShapelessRecipe(new ItemStack(Item.lightStoneDust, 8), new ItemStack(Item.gunpowder), Items.getItem("coalDust"));

        LanguageRegistry.addName(itemCandy, COLOUR_CYAN + "Candy");
        GameRegistry.registerItem(itemCandy, "itemCandy");

        LanguageRegistry.addName(blockUUMatter, COLOUR_PURPLE + "UU-Matter Block");
        GameRegistry.registerBlock(blockUUMatter, "blockUUMatter");

        if (Loader.isModLoaded("ThermalExpansion")) {

            System.out.println("[Fixed Recipes] Thermal Expansion conversion activated!");

            try {

                convertRecipe(Items.getItem("ironDust"), ItemRegistry.getItem("dustIron", 1));
                convertRecipe(Items.getItem("goldDust"), ItemRegistry.getItem("dustGold", 1));
                //convertRecipe(Items.getItem("obsidianDust"), ItemRegistry.getItem("dustObsidian", 1));
                convertRecipe(Items.getItem("copperDust"), ItemRegistry.getItem("dustCopper", 1));
                convertRecipe(Items.getItem("tinDust"), ItemRegistry.getItem("dustTin", 1));
                convertRecipe(Items.getItem("silverDust"), ItemRegistry.getItem("dustSilver", 1));
                convertRecipe(Items.getItem("ironDust"), ItemRegistry.getItem("dustIron", 1));
                convertRecipe(Items.getItem("bronzeDust"), ItemRegistry.getItem("dustBronze", 1));

                convertRecipe(Items.getItem("copperIngot"), ItemRegistry.getItem("ingotCopper", 1));
                convertRecipe(Items.getItem("tinIngot"), ItemRegistry.getItem("ingotTin", 1));

                convertRecipe(Items.getItem("copperBlock"), ItemRegistry.getItem("blockCopper", 1));
                convertRecipe(Items.getItem("tinBlock"), ItemRegistry.getItem("blockTin", 1));

                GameRegistry.addRecipe(ItemRegistry.getItem("oreSilver", 4), new Object[]{" U ", "   ", " U ", 'U', blockUUMatter});

            } catch (Exception e) {
                System.out.println("Fixed Recipes: Thermal Expansion integration was unsuccessful - please contact the author of this mod to let them know that the API may have changed.");
            }
        } else {
            System.out.println("Thermal Expansion conversion de-activated!");
        }

    }

    @Mod.PostInit
    public void postInit(FMLPostInitializationEvent event) {

    }

}
